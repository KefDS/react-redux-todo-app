import React from 'react';
import ReactDOM from 'react-dom';

export default class TextInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: props.text}
  }

  handleKeyDown(e) {
    switch (e.key) {
      case 'Enter':
        return this.props.doneEditing(this.props.itemId, this.state.value);
      case 'Escape':
        return this.cancelEditing();
    }
  }

  handleOnBlur(e) {
    return this.cancelEditing();
  }

  handleOnChange(e) {
    this.setState({value: e.target.value});
  }

  cancelEditing() {
    this.setState({value: this.props.text});
    return this.props.cancelEditing(this.props.itemId);
  }

  render() {
    return (
      <input className="edit"
        autoFocus={true}
        value={this.state.value}
        onChange={this.handleOnChange.bind(this)}
        type="text"
        ref="itemInput"
        onKeyDown={this.handleKeyDown.bind(this)}
        onBlur={this.handleOnBlur.bind(this)} />
    );
  }
};
