import React from 'react';
import TodoList from './TodoList';
import TodoHeader from './TodoHeader';
import TodoTools from './TodoTools';
import Footer from './Footer';
import {connect} from 'react-redux';

import * as actionCreators from '../actionCreators';

export class TodoApp extends React.Component {
  getNumberActiveItems() {
    return this.props.todos.filter(item =>
      item.get('status') === 'active'
    ).size;
  }

  render() {
    return (
      <div>
        <section className="todoapp">
          <TodoHeader addItem={this.props.addItem} />
          <TodoList {...this.props} />
          <TodoTools changeFilter={this.props.changeFilter}
            filter={this.props.filter}
            numberActiveItems={this.getNumberActiveItems()}
            clearCompleted={this.props.clearCompleted} />
        </section>
        <Footer />
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
    todos: state.get('todos'),
    filter: state.get('filter')
  };
}

export const TodoAppContainer = connect(mapStateToProps, actionCreators)(TodoApp);
