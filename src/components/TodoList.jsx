import React from 'react';
import TodoItem from './TodoItem';

export default class TodoList extends React.Component {
  getItems() {
    return this.props.filter === 'all' ?
      this.props.todos :
      this.props.todos.filter(item =>
        item.get('status') === this.props.filter
      );
  }

  isCompleted(item) {
    return item.get('status') === 'completed';
  }

  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.getItems().map(item =>
            <TodoItem
              id={item.get('id')}
              key={item.get('text')}
              text={item.get('text')}
              isCompleted={this.isCompleted(item)}
              isEditing={item.get('editing')}
              doneEditing={this.props.doneEditing}
              cancelEditing={this.props.cancelEditing}
              toggleComplete={this.props.toggleComplete}
              deleteItem={this.props.deleteItem}
              editItem={this.props.editItem} />
          )}
        </ul>
      </section>
    );
  }
};
