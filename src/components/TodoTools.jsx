import React from 'react';
import classNames from 'classnames';

export default class TodoTools extends React.Component {
  getNumberItemsLeft() {
    return this.props.numberActiveItems || 0;
  }

  isSelected(filter) {
    return this.props.selectedFilter === filter || false;
  }

  setSelectedClass(filter) {
    return classNames({
      'selected': this.props.filter === filter
    });
  }

  render() {
    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{this.getNumberItemsLeft()}</strong> items left
          </span>
          <ul className="filters">
            {['All', 'Active', 'Completed'].map((name, index) =>
              <li>
                <a href="#"
                  onClick={() => this.props.changeFilter(name.toLowerCase())}
                  className={this.setSelectedClass(name.toLowerCase())} >
                  {name}
                </a>
              </li>
            )}
          </ul>
          <button className="clear-completed"
            onClick={this.props.clearCompleted}>
            Clear completed
          </button>
        </footer>
      );
    }
  };
