import React from 'react';
import TestUtils from 'react-addons-test-utils';
import TextInput from '../../src/components/TextInput';
import {expect} from 'chai';

const {renderIntoDocument,
  scryRenderDOMComponentWithTag,
  Simulate} = TestUtils;

describe('TextInput', () => {
  const text = 'React';

  it('calls a callback when pressing enter', () => {
    var hasDoneEditing = false;

    const doneEditing = () => hasDoneEditing = true;
    const component = renderIntoDocument(
      <TextInput text={text} doneEditing={doneEditing} />
    );

    const input = component.refs.itemInput;
    Simulate.keyDown(input, {key: 'Enter', keyCode: 13, which: 13});

    expect(hasDoneEditing).to.equal(true);
  });

  it('call a callback when pressing escape or losing focus', () => {
    var hasCancelEditing = false;
    const cancelEditing = () => hasCancelEditing = true;
    const component = renderIntoDocument(
      <TextInput text={text} cancelEditing={cancelEditing} />
    );
    const input = component.refs.itemInput;
    Simulate.keyDown(input, {key: 'Escape', keyCode: 27, which: 27});

    expect(hasCancelEditing).to.equal(true);
  });
});
